import os
from setuptools import setup

# Utility function to read the README file.
# Used for the long_description.  It's nice, because now 1) we have a top level
# README file and 2) it's easier to type in the README file than to put a raw
# string in below ...
def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()

setup(
    name = "python_shg",
    version = "2.0.0",
    author = "Joel Collins",
    author_email = "joel@jtcollins.net",
	install_requires=['numpy', 'scipy', 'matplotlib'],
    description = ("Analysis module and scripts for University of Bath MPNP SHG experiments"),
    license = "GNU GPL",
    keywords = "data analysis physics",
    url = "https://github.com/jtc42/mpnp-shg-analysis",
    packages=['python_shg'],
    long_description=read('README.md'),
    classifiers=[
        "Development Status :: 5 - Production/Stable",
        "Topic :: Scientific/Engineering :: Physics",
        "License :: OSI Approved :: GNU General Public License v3 (GPLv3)",
    ],
)