# python_shg
[MPNP] Analysis module and scripts for SHG experiments

## Requirements
* Numpy
* Scipy
* Matplotlib

## Installation
Run ```python setup.py install```

## Examples

Place data files in a subfolder named 'data', alongside either 'analyse_cp.py' or 'analyse_or.py'.

Assuming file names follow the default format from the LabView program, the script will automatically find the required information, and generate plots.
