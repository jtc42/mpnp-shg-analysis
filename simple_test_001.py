import glob
import python_shg as shg

### LOAD DATA ###

# Find data files (consider separating CP and OR files)
files = glob.glob("data/*.shg") #Find all data files

# Create dataset (consider separating CP and OR data)
dataset = shg.make_dataset(files)


### ANALYZE ###

# Create Pol0, Ana0 filtered dataset
dataset_PinPout = shg.data_filter(dataset, {'Pol':0, 'Ana': 0})
        
# Create a 2D data matrix of the filtered dataset, by QWP and Sample angles
matrix = shg.make_2d(dataset_PinPout, *['QWP', 'Sample'])
        
# Create plot

# If data matrix is not empty
if matrix: 
    # Make plot
    fig = shg.heatmap(matrix, "QWP angle", "Sample angle", title="P in P out")
    fig.show()
else:
    print("No matching data")
