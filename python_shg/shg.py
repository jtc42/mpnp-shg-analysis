import os
import copy
import datetime
import numpy as np
import matplotlib.pyplot as plt

plt.ioff()

# TODO: Conversion of old format datafiles into new format. Submodule shg.convert.txt2shg(file)
# TODO: OR angle-fits in a sub-module shg.fit.()???


# Create a bank of data points from a single file
def make_databank(path, header=1, rounding = 3):
    # Get x and y data
    x_data, y_data = np.loadtxt(path, skiprows=header, unpack=True, delimiter=',')

    # Get header data
    with open(path) as file:
        header = file.readline()

    # Get timestamp
    filename = os.path.basename(path).lower()
    dt_name = filename.split('_')[:2]
    dt_array = [dt_name[0][0:4], dt_name[0][4:6], dt_name[0][6:8], dt_name[1][0:2], dt_name[1][3:5], dt_name[1][6:8]]

    dt_value = datetime.datetime(*map(int, dt_array))

    # Split header data into an array (skipping 'metadata' tag)
    header_exclusions = ['metadata', 'Metadata', '\n']
    header_array = [val.split('=') for val in header.split(';') if val not in header_exclusions]

    params = []
    for h in header_array:
        # Convert integer header arguments from strings
        try:
            h[1] = int(h[1])
        except:
            pass

        # Find parameters corresponding to x and y data, and store all others
        try: # If no 1st entry exists
            if h[1] == 'x':
                x_arg = h[0]
            elif h[1] == 'y':
                y_arg = h[0]
            else:
                params.append(h)
        except:
            pass

    # Create databank dictionary
    databank = []

    # For each x value in file
    for i, x in enumerate(x_data):
        param_dict = {'datetime' : dt_value}

        # For each global parameter within file
        for p in params:
            param_dict[p[0]] = p[1]

        param_dict[x_arg] = x_data[i]
        param_dict[y_arg] = round(y_data[i], rounding)

        databank.append(param_dict) # Append new datapoint dictionary to list

    return databank


# Create a set of data points from multiple files
def make_dataset(file_list, header=1, rounding=3):
    print("Creating dataset of data point dictionaries. Dict keys found from datafile header.")
    dataset = []

    for file in file_list:
        d = make_databank(file, header=header, rounding=rounding)
        dataset += d # Attach new list of datapoint dictionaries to dataset list

    return dataset


# FILTERING

# Sort a list of data points by a parameter key
def data_sort(data_list, sort_key):
    return [d for (s,d) in sorted(zip([d[sort_key] for d in data_list], data_list))]


# Return filtered dataset reduced by dictionary of conditions
# Eg condition_dict = {'Pol': 0, 'Ana': 90}
def data_filter(dataset, condition_dict):
    filtered = copy.copy(dataset) # Make an initial copy of the argument list, for modifying

    for key, value in condition_dict.items():
        filtered = [d for d in filtered if (key in d.keys() and d[key] == value)] # Apply filter to copied list

    return filtered


# Return sorted list of all unique 'param' values in 'dataset'
def data_unique(dataset, param):
    vals_list = list( set([d[param] for d in dataset]) )
    vals_list.sort()
    return vals_list


# Make 2D dataset ready for plotting. returns list of [x vals, y vals, matrix]
# Assumes dataset is already filtered!
def make_2d(dataset, x_param, y_param, z_param='CTS'):
    print("Sorting x and y values...")
    y_vals = data_unique(dataset, y_param) # Sorted list of all y values
    x_vals = data_unique(dataset, x_param) # Sorted list of all x values

    matrix = []
    contains_data = False # Flag to see if filter returns any results ever

    print("Scanning dataset for matches...")
    for y in y_vals: # For each y value
        data_at_y = data_filter(dataset, {y_param: y}) # Make array of datapoints with that y value

        if len(data_at_y) > 0: # If points exist after filtering
            contains_data = True # Experiment contains data
            data_at_y = data_sort(data_at_y, x_param) # Sort the array by x value
            matrix.append([datapoint[z_param] for datapoint in data_at_y])

    # Return only if experiment contains results
    if contains_data:
        return [x_vals, y_vals, matrix]
    else:
        return None


# MODIFYING

def construct_average(dataset, metadata_index=0, weights=None):
    data_avg = copy.copy(dataset[0])
    cts_avg = np.average([d['CTS'] for d in dataset], weights=weights)
    data_avg['CTS'] = cts_avg
    return data_avg


# PLOTTING

def heatmap(data, x_label, y_label, title='Untitled'):
    # Data should be in format [x_vals, y_vals, matrix]

    fig = plt.figure()

    plt.pcolormesh(data[0], data[1], data[2], cmap='hot')
    plt.title(title)

    # Set the limits of the plot to the limits of the data
    # TODO: Axis parameters should come from dataset. Won't always be 0-360
    plt.axis([0, 360, 0, 360])
    plt.xticks(np.arange(0, 405, 45))
    plt.yticks(np.arange(0, 405, 45))

    plt.colorbar()
    plt.ylabel(y_label)
    plt.xlabel(x_label)

    return fig

# TODO: 2D plot routines
