import glob
import python_shg as shg

# Find data files (consider separating CP and OR files)
files = glob.glob("data/*.shg") #Find all data files

# Create dataset (consider separating CP and OR data)
dataset = shg.make_dataset(files)

# Dictionary of parameters for different types of experiment
experiment_params = {
        'continuous_polarisation': {
                'plt_xy': ['QWP', 'Sample'], 
                'filters': [{'Pol':0, 'Ana': 0}, {'Pol':0, 'Ana': 90}]},

        'optical_rotation': {
                'plt_xy': ['Ana', 'Sample'], 
                'filters': [{'Pol':0, 'QWP': 0}, {'Pol':90, 'QWP': 0}]},
        }


# Basic function to make an experiment
def make_experiment(dataset, plt_xy, filters, prefix=''):
    # For each filter
    for fltr in filters:
        # Create the filtered dataset
        print("Applying filter to dataset...")
        filtered = shg.data_filter(dataset, fltr)
        
        # Create the 2D data matrix of the filtered dataset, by QWP and Sample angles
        print("Shaping filtered data into plot matrix...")
        matrix = shg.make_2d(filtered, *plt_xy)
        
        # Create title
        print("Creating plot...")
        title = prefix+",".join(["{} {}deg".format(key, value) for key, value in fltr.items()])
        
        # If experiment contains matching data
        if matrix: 
            # Make plot
            fig = shg.heatmap(matrix, *['{} Angle'.format(axis) for axis in plt_xy], title=title)
            fig.show()
            fig.savefig(title+'.png')
        else:
            print("No matching data for "+title)


# BASIC CONTINUOUS POLARISATION EXPERIMENT
make_experiment(dataset, experiment_params['continuous_polarisation']['plt_xy'],
                experiment_params['continuous_polarisation']['filters'],
                prefix='ContPol_Example')

# BASIC OPTICAL ROTATION EXPERIMENT
make_experiment(dataset,
                experiment_params['optical_rotation']['plt_xy'],
                experiment_params['optical_rotation']['filters'],
                prefix='OptRot_Example')
